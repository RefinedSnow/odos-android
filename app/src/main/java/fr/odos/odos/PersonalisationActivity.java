package fr.odos.odos;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import fr.odos.odos.model.Data;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PersonalisationActivity extends AppCompatActivity {

    int MAXSKIN = Data.getInstance().getMaxSkin();
    int MAXHAIR = Data.getInstance().getMaxHair();
    int MAXCLOTHES = Data.getInstance().getMaxClothes();

    ArrayList<String> skinName = new ArrayList<>(MAXSKIN);
    ArrayList<String> hairName = new ArrayList<>(MAXHAIR);
    ArrayList<String> clothesName = new ArrayList<>(MAXCLOTHES);

    int iSkin = 0;
    int iHair = 0;
    int iClothes = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalisation);


        final Button bBack = (Button)  findViewById(R.id.bBack);
        bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalisationActivity.this, RegisterActivity.class);
                finish();
                startActivity(intent);
            }
        });


        final Button bSave = (Button)  findViewById(R.id.bSave);
        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
                Intent intent = new Intent(PersonalisationActivity.this, MapActivity.class);
                finish();
                startActivity(intent);
            }
        });

        skinName = Data.getInstance().get_skinList();
        hairName = Data.getInstance().get_hairList();
        clothesName = Data.getInstance().get_clothesList();


        final Button bHairLeft = (Button) findViewById(R.id.bHairLeft);
        final Button bHairRight = (Button) findViewById(R.id.bHairRight);
        final Button bSkinLeft = (Button) findViewById(R.id.bSkinLeft);
        final Button bSkinRight = (Button) findViewById(R.id.bSkinRight);
        final Button bClothesLeft = (Button) findViewById(R.id.bClothesLeft);
        final Button bClothesRight = (Button) findViewById(R.id.bClothesRight);


        bHairLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change(1,-1);
            }
        });

        bHairRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change(1,1);
            }
        });

        bSkinLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change(2,-1);
            }
        });

        bSkinRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change(2,1);
            }
        });

        bClothesLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change(3,-1);
            }
        });

        bClothesRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change(3,1);
            }
        });
    }


    private void change(int type, int direction){

        if(type == 1){
            iHair = iHair + direction;
            if(iHair < 0){
                iHair = MAXHAIR -1;
            }
            if(iHair == MAXHAIR){
                iHair = 0;
            }
            ImageView iv = (ImageView) findViewById(R.id.iHair);
            int img = getResources().getIdentifier(hairName.get(iHair), "drawable", getPackageName());
            try{
                Drawable d = getResources().getDrawable(img, null);
                iv.setImageDrawable(d);
                return;
            }catch (Exception e){
                Toast.makeText(this, "@string/problemImage", Toast.LENGTH_SHORT).show();
            }
        } else if(type == 2){
            iSkin = iSkin + direction;
            if(iSkin < 0){
                iSkin = MAXSKIN-1;
            }
            if(iSkin == MAXSKIN){
                iSkin = 0;
            }
            ImageView iv = (ImageView) findViewById(R.id.iFace);
            int img = getResources().getIdentifier(skinName.get(iSkin), "drawable", getPackageName());
            try{
                Drawable d = getResources().getDrawable(img, null);
                iv.setImageDrawable(d);
                return;
            }catch (Exception e){
                Toast.makeText(this, "@string/problemImage", Toast.LENGTH_SHORT).show();
            }
        } else {
            iClothes = iClothes + direction;
            if(iClothes < 0){
                iClothes = MAXCLOTHES-1;
            }
            if (iClothes == MAXCLOTHES){
                iClothes = 0;
            }
            ImageView iv = (ImageView) findViewById(R.id.iClothes);
            int img = getResources().getIdentifier(clothesName.get(iClothes), "drawable", getPackageName());
            try{
                Drawable d = getResources().getDrawable(img, null);
                iv.setImageDrawable(d);
                return;
            }catch (Exception e){
                Toast.makeText(this, "@string/problemImage", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void save(){


        MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient okHttpClient = new OkHttpClient();

        JSONObject json = new JSONObject();
        try {
            json.put("email", Data.getInstance().get_email());
            json.put("skin", iSkin);
            json.put("hair", iHair);
            json.put("shirt", iClothes);
            Request myGetRequest = new Request.Builder()
                    .url("https://dome7.ensicaen.fr:8443/api/setAvatar")
                    .post(RequestBody.create(JSON_TYPE, json.toString()))
                    .build();

            okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    final String text = response.body().string();
                    try {

                        JSONObject obj = new JSONObject(text);
                        String val =  obj.getString("success");

                        if (val == "true"){
                            Intent intent = new Intent(PersonalisationActivity.this, MapActivity.class);
                            finish();
                            startActivity(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(LoginActivity.this, text, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
