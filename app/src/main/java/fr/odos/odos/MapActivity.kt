package fr.odos.odos

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.PolylineOptions
import fr.odos.odos.GPS.*
import fr.odos.odos.model.Data
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

public class MapActivity : AppCompatActivity(), OnMapReadyCallback, IGPS, IRoute, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener, ReportFragment.ReportCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var gps: GPSService
    private lateinit var route: RouteManager
    private lateinit var currentLocation: GPSLocation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val email:String = Data.getInstance()._email
        val JSON_TYPE = MediaType.parse("application/json; charset=utf-8")
        val okHttpClient = OkHttpClient()
        val json = JSONObject()


        try {
            json.put("email", email)
            val myGetRequest = Request.Builder()
                    .url("https://dome7.ensicaen.fr:8443/api/user")
                    .post(RequestBody.create(JSON_TYPE, json.toString()))
                    .build()

            okHttpClient.newCall(myGetRequest).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {

                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    val text = response.body()!!.string()
                    try {

                        val obj = JSONObject(text)
                        Data.getInstance().pseudo = obj.getString("nickname")
                        Data.getInstance().distance = Integer.parseInt(obj.getString("distance"))
                        Data.getInstance().score = Integer.parseInt(obj.getString("score"))

                        Data.getInstance().pet = Integer.parseInt(obj.getString("pet"))

                        val avatar = obj.getJSONArray("avatar")
                        Data.getInstance().skin = Integer.parseInt(avatar.getString(0))
                        Data.getInstance().hair = Integer.parseInt(avatar.getString(1))
                        Data.getInstance().shirt = Integer.parseInt(avatar.getString(2))


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    runOnUiThread {
                        initViews()
                        //Toast.makeText(LoginActivity.this, text, Toast.LENGTH_SHORT).show();
                    }
                }
            })

        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun initViews() {
        val listPet = Data.getInstance()._petList
        val skinName = Data.getInstance()._skinList
        val hairName = Data.getInstance()._hairList
        val clothesName = Data.getInstance()._clothesList

        val tVPseudo = findViewById<View>(R.id.tVPseudo) as TextView
        val tVDistance = findViewById<View>(R.id.tVDistance) as TextView
        val tVCoin = findViewById<View>(R.id.tVCoin) as TextView

        tVPseudo.text = Data.getInstance().pseudo
        tVDistance.text = Data.getInstance().distance.toString()
        tVCoin.text = Data.getInstance().score.toString()

        var iv: ImageView? = null
        var img = 0
        var d: Drawable? = null

        iv = findViewById<View>(R.id.iPet) as ImageView
        img = resources.getIdentifier(listPet[Data.getInstance().pet], "drawable", packageName)
        d = resources.getDrawable(img, null)
        iv.setImageDrawable(d)

        iv = findViewById<View>(R.id.iHair) as ImageView
        img = resources.getIdentifier(hairName[Data.getInstance().hair], "drawable", packageName)
        d = resources.getDrawable(img, null)
        iv.setImageDrawable(d)

        iv = findViewById<View>(R.id.iFace) as ImageView
        img = resources.getIdentifier(skinName[Data.getInstance().skin], "drawable", packageName)
        d = resources.getDrawable(img, null)
        iv.setImageDrawable(d)

        iv = findViewById<View>(R.id.iShirt) as ImageView
        img = resources.getIdentifier(clothesName[Data.getInstance().shirt], "drawable", packageName)
        d = resources.getDrawable(img, null)
        iv.setImageDrawable(d)


        val iFace = findViewById<View>(R.id.iFace) as ImageView
        iFace.setOnClickListener {
            val intent = Intent(this@MapActivity, ProfileActivity::class.java)
            finish()
            startActivity(intent)
        }

        val tPseudo = findViewById<View>(R.id.tVPseudo) as TextView
        tPseudo.setOnClickListener {
            val intent = Intent(this@MapActivity, ProfileActivity::class.java)
            finish()
            startActivity(intent)
        }

        val iLight = findViewById<View>(R.id.iLight) as ImageView
        iLight.setOnClickListener {
            val intent = Intent(this@MapActivity, PetActivity::class.java)
            finish()
            startActivity(intent)
        }


    }

    override fun onResume() {
        super.onResume()
        val btnReport = findViewById<Button>(R.id.btnReport)
        btnReport.visibility = View.VISIBLE
    }

    fun requestPermissions() {
        val permissionCheckFine = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
        val permissionCheckCoarse = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)

        if (permissionCheckFine == PackageManager.PERMISSION_DENIED || permissionCheckCoarse == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 0)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        var style = MapStyleOptions.loadRawResourceStyle(this, R.raw.style_custom)
        mMap.setMapStyle(style)

        gps = GPSService()
        requestPermissions()
        mMap.setOnMyLocationButtonClickListener(this)
        mMap.setOnMyLocationClickListener(this)
        enableMyLocation()

        gps.getGPSLocation(applicationContext)
        gps.setGPSListener(this)
    }

    @SuppressLint("MissingPermission")
    private fun enableMyLocation() {
        if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.isMyLocationEnabled = true
        }
    }


    override fun onRouteChanged(lineOptions: PolylineOptions) {
        mMap.addPolyline(lineOptions)
    }

    override fun onLocationChange(gpsLocation: GPSLocation) {
        if (gpsLocation != null) {

            //mMap.addMarker(MarkerOptions().position(LatLng(gpsLocation.longitude, gpsLocation.latitude)).title("Marker"))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(gpsLocation.longitude, gpsLocation.latitude)))
            var zoom = CameraUpdateFactory.zoomTo(16.0F)
            mMap.animateCamera(zoom)
            currentLocation = gpsLocation
            route = RouteManager(mMap, currentLocation)
        }

    }

    fun launchReport(view: View) {

        // Create new fragment and transaction
        val newFragment = ReportFragment()
        val transaction = fragmentManager.beginTransaction()


// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
        transaction.replace(R.id.map_container, newFragment)
        transaction.addToBackStack(null)

// Commit the transaction
        transaction.commit()

        view.visibility = View.INVISIBLE
    }

    override fun onReport(description: String, type: String) {
        val JSON_TYPE = MediaType.parse("application/json; charset=utf-8")
        val okHttpClient = OkHttpClient()
        val json = JSONObject()

        try {
            json.put("type", type)
            json.put("description", description)
            val jsonCoords = JSONObject()
            jsonCoords.put("lat", currentLocation.latitude)
            jsonCoords.put("lng", currentLocation.longitude)


            var jsonEvent = JSONObject()
            jsonEvent.put("event", json)
            jsonEvent.put("coords", jsonCoords)

            Log.d("PIZZA", jsonEvent.toString())

            val myGetRequest = Request.Builder()
                    .url("https://dome7.ensicaen.fr:8443/api/addEvent")
                    .post(RequestBody.create(JSON_TYPE, jsonEvent.toString()))
                    .build()


            if (description.isNotEmpty() && type.isNotEmpty()) {
                okHttpClient.newCall(myGetRequest).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {

                    }

                    @Throws(IOException::class)
                    override fun onResponse(call: Call, response: Response) {
                        val text = response.body()!!.string()
                        try {

                            /* val obj = JSONObject(text)
                        val `val` = obj.getString("success")
                        if (`val` === "true") {
                           /*val intent = Intent(this@LoginActivity, ProfileActivity::class.java)
                            startActivity(intent)*/
                        }*/

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        //runOnUiThread(Toast.makeText(applicationContext, "Signalement reussi", Toast.LENGTH_SHORT)::show)
                    }
                })
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val intent = Intent(this@MapActivity, MapActivity::class.java)
        finish()
        startActivity(intent)
    }

    override fun onMyLocationButtonClick(): Boolean {
        //Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show()

        gps.getGPSLocation(applicationContext)
        gps.setGPSListener(this)
        return false
    }

    override fun onMyLocationClick(location: Location) {
        //Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show()
    }

}
