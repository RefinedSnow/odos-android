package fr.odos.odos;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import fr.odos.odos.model.Data;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PetActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet);

        int PRIXMINI = 25;
        int PRIXMID = 50;
        int PRIXMAX = 100;

        Button bSetting = (Button) findViewById(R.id.bSetting);
        bSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveScore();
                savePet();
                Intent intent = new Intent(PetActivity.this, SettingsActivity.class);
                finish();
                startActivity(intent);
            }
        });

        Button bBack = (Button) findViewById(R.id.bBack);
        bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveScore();
                savePet();
                Intent intent = new Intent(PetActivity.this, MapActivity.class);
                finish();
                startActivity(intent);
            }
        });


        String email = Data.getInstance().get_email();
        final TextView tVNbCoin =  (TextView) findViewById(R.id.tVNbCoin);



        tVNbCoin.setText(String.valueOf(Data.getInstance().getScore()));

        final Button b1 = (Button) findViewById(R.id.b1);
        final Button b2 = (Button) findViewById(R.id.b2);
        final Button b3 = (Button) findViewById(R.id.b3);
        final Button b4 = (Button) findViewById(R.id.b4);
        final Button b5 = (Button) findViewById(R.id.b5);
        final Button b6 = (Button) findViewById(R.id.b6);
        final Button b7 = (Button) findViewById(R.id.b7);
        final Button b8 = (Button) findViewById(R.id.b8);
        final Button b9 = (Button) findViewById(R.id.b9);



        final TextView coins = (TextView) findViewById(R.id.tVNbCoin);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b1, coins,1)){
                    ImageView iv = (ImageView) findViewById(R.id.i1);
                    changeImage(b1, "dragonra",iv,coins,1);
                }
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b2, coins,2)){
                    ImageView iv = (ImageView) findViewById(R.id.i2);
                    changeImage(b2, "dragonrb",iv,coins,2);
                }
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b3, coins,3)){
                    ImageView iv = (ImageView) findViewById(R.id.i3);
                    changeImage(b3, "dragonrc",iv,coins,3);
                }
            }
        });

        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b4, coins,4)){
                    ImageView iv = (ImageView) findViewById(R.id.i4);
                    changeImage(b4, "dragonva",iv,coins,4);
                }
            }
        });

        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b5, coins,5)){
                    ImageView iv = (ImageView) findViewById(R.id.i5);
                    changeImage(b5, "dragonvb",iv,coins,5);
                }
            }
        });

        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b6, coins,6)){
                    ImageView iv = (ImageView) findViewById(R.id.i6);
                    changeImage(b6, "dragonvc",iv,coins,6);
                }
            }
        });

        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b7, coins,7)){
                    ImageView iv = (ImageView) findViewById(R.id.i7);
                    changeImage(b7, "dragonna",iv,coins,7);
                }
            }
        });

        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b8, coins,8)){
                    ImageView iv = (ImageView) findViewById(R.id.i8);
                    changeImage(b8, "dragonnb",iv,coins,8);
                }
            }
        });

        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enoughCoin(b9, coins,9)){
                    ImageView iv = (ImageView) findViewById(R.id.i9);
                    changeImage(b9, "dragonnc",iv,coins,9);
                }
            }
        });

        ArrayList<Integer> listPet = Data.getInstance().get_stateButtonPet();

        b1.setText("25");
        b2.setText("50");
        b3.setText("100");
        b4.setText("25");
        b5.setText("50");
        b6.setText("100");
        b7.setText("25");
        b8.setText("50");
        b9.setText("100");

        if(listPet.get(1) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i1);
            initUnlock(b1,listPet,"dragonra",iv,coins,1);
        }
        if(listPet.get(2) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i2);
            initUnlock(b2,listPet,"dragonrb",iv,coins,2);
        }
        if(listPet.get(3) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i3);
            initUnlock(b3,listPet,"dragonrc",iv,coins,3);
        }
        if(listPet.get(4) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i4);
            initUnlock(b4,listPet,"dragonva",iv,coins,4);
        }
        if(listPet.get(5) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i5);
            initUnlock(b5,listPet,"dragonvb",iv,coins,5);
        }
        if(listPet.get(6) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i6);
            initUnlock(b6,listPet,"dragonvc",iv,coins,6);
        }
        if(listPet.get(7) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i7);
            initUnlock(b7,listPet,"dragonna",iv,coins,7);
        }
        if(listPet.get(8) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i8);
            initUnlock(b8,listPet,"dragonnb",iv,coins,8);
        }
        if(listPet.get(9) != 0) {
            ImageView iv = (ImageView) findViewById(R.id.i9);
            initUnlock(b9,listPet,"dragonnb",iv,coins,9);
        }


    }

    private  void initUnlock(Button b, ArrayList<Integer> listPet, String nom,ImageView iv, TextView coins, int num){
        changeImage(b, nom,iv,coins,num);
        if(listPet.get(1) == 1){
            b.setText("Obtenu");
        }else {
            b.setText("Choisi");
        }
    }

    private void savePet() {
        String email = Data.getInstance().get_email();
        final TextView tVNbCoin =  (TextView) findViewById(R.id.tVNbCoin);

        MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient okHttpClient = new OkHttpClient();

        JSONObject json = new JSONObject();

        try {
            json.put("email", email);
            json.put("pet",Data.getInstance().getPet() );
            Request myGetRequest = new Request.Builder()
                    .url("https://dome7.ensicaen.fr:8443/api/setscore")
                    .post(RequestBody.create(JSON_TYPE, json.toString()))
                    .build();

            okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveScore() {
        String email = Data.getInstance().get_email();
        final TextView tVNbCoin =  (TextView) findViewById(R.id.tVNbCoin);

        MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient okHttpClient = new OkHttpClient();

        JSONObject json = new JSONObject();


        try {
            json.put("email", email);
            json.put("score",Data.getInstance().getScore() );
            Request myGetRequest = new Request.Builder()
                    .url("https://dome7.ensicaen.fr:8443/api/setscore")
                    .post(RequestBody.create(JSON_TYPE, json.toString()))
                    .build();

            okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void changeImage(Button b, String nomImage, ImageView iv, TextView coins, int numPet){
        if(Data.getInstance().get_stateButtonPet().get(numPet) == 1){
            b.setText("Choisi");
            int pet = Data.getInstance().getPet();
            //saveUnlock(numPet);
            changeText(pet);
            Data.getInstance().setPet(numPet);
            Data.getInstance().get_stateButtonPet().set(numPet,2);
            return;
        }else if(Data.getInstance().get_stateButtonPet().get(numPet) ==  2 ){
            Data.getInstance().setPet(0);
            b.setText("Obtenu");
            Data.getInstance().get_stateButtonPet().set(numPet,1);
            return;
        }else{
            int img = getResources().getIdentifier(nomImage, "drawable", getPackageName());
            try{
                Drawable d = getResources().getDrawable(img, null);
                iv.setImageDrawable(d);
                int coin = Integer.parseInt(coins.getText().toString());
                int prix = Integer.parseInt(b.getText().toString());
                coin = coin - prix;
                coins.setText(String.valueOf(coin));
                Data.getInstance().setScore(coin);
                Data.getInstance().get_stateButtonPet().set(numPet,1);
                b.setText("Obtenu");
                return;
            }catch (Exception e){
            }
        }

    }

    private void saveUnlock(int numPet) {
        String email = Data.getInstance().get_email();
        final TextView tVNbCoin =  (TextView) findViewById(R.id.tVNbCoin);

        MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient okHttpClient = new OkHttpClient();

        JSONObject json = new JSONObject();


        try {
            json.put("email", email);
            json.put("petNumber", numPet);
            Request myGetRequest = new Request.Builder()
                    .url("https://dome7.ensicaen.fr:8443/api/petUnlock")
                    .post(RequestBody.create(JSON_TYPE, json.toString()))
                    .build();

            okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void changeText(int pet) {
        final Button b1 = (Button) findViewById(R.id.b1);
        final Button b2 = (Button) findViewById(R.id.b2);
        final Button b3 = (Button) findViewById(R.id.b3);
        final Button b4 = (Button) findViewById(R.id.b4);
        final Button b5 = (Button) findViewById(R.id.b5);
        final Button b6 = (Button) findViewById(R.id.b6);
        final Button b7 = (Button) findViewById(R.id.b7);
        final Button b8 = (Button) findViewById(R.id.b8);
        final Button b9 = (Button) findViewById(R.id.b9);
        Data.getInstance().get_stateButtonPet().set(pet,1);
        switch (pet){
            case (1):
                b1.setText("Obtenu");
                break;
            case (2):
                b2.setText("Obtenu");
                break;
            case (3):
                b3.setText("Obtenu");
                break;
            case (4):
                b4.setText("Obtenu");
                break;
            case (5):
                b5.setText("Obtenu");
                break;
            case (6):
                b6.setText("Obtenu");
                break;
            case (7):
                b7.setText("Obtenu");
                break;
            case (8):
                b8.setText("Obtenu");
                break;
            case (9):
                b9.setText("Obtenu");
                break;
        }
    }


    @NonNull
    private Boolean enoughCoin(Button b, TextView coins, int numPet){
        if(Data.getInstance().get_stateButtonPet().get(numPet) != 0){
            return true;
        }else{
            int coin = Integer.parseInt(coins.getText().toString());
            if(coin - Integer.parseInt(b.getText().toString()) >= 0){
                return true;
            }
        }
        Toast.makeText(this, "Vous n'avez pas assez de points", Toast.LENGTH_SHORT).show();
        return false;
    }
}
