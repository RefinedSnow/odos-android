package fr.odos.odos

import android.app.Fragment
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.app.Activity
import android.support.annotation.Nullable
import android.widget.Button


/**
 * Created by Jen on 18.01.2018.
 */

class ReportFragment : Fragment(){

    lateinit var etType : EditText
    lateinit var etDesc : EditText

    lateinit var mCallback: ReportCallback


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view =  inflater.inflate(R.layout.fragment_report, container, false)
        etType = view.findViewById(R.id.etType) as EditText
        etDesc = view.findViewById(R.id.etReportDesc) as EditText

        return view
    }

    override fun onActivityCreated(@Nullable savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //getActivity() is fully created in onActivityCreated and instanceOf differentiate it between different Activities
        if (activity is ReportCallback)
            mCallback = activity as ReportCallback
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val btn = view.findViewById(R.id.btnReport) as Button
        btn.setOnClickListener({
            if (mCallback != null)
                mCallback.onReport(etDesc.text.toString(),etType.text.toString())
        })
    }

    interface ReportCallback {
        fun onReport(description:String, type:String) {

        }
    }


}