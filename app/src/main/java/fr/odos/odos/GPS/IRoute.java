package fr.odos.odos.GPS;

import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by gabon on 17/01/18.
 */

public interface IRoute {
    public void onRouteChanged(PolylineOptions lineOptions);
}
