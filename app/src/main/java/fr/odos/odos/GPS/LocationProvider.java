package fr.odos.odos.GPS;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;


public class LocationProvider {

    private static int cpt;

    /**
     * The location callback interface.
     */
    public static interface LocationCallback {
        public void onNewLocationAvailable(GPSCoordinates location);
    }


    /**
     * Requests the location just once.
     * @param context The application context.
     * @param callback The location callback.
     */
    public static void requestSingleUpdate(final Context context, final LocationCallback callback) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        cpt = 100;

            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates(0, 0, criteria, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        while (cpt > 0){
                            callback.onNewLocationAvailable(new GPSCoordinates(location.getLongitude(), location.getLatitude(), location.getAltitude()));
                            locationManager.removeUpdates(this);
                            cpt--;
                        }

                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }
                }, null);
            }
    }

    public static class GPSCoordinates {
        public float longitude = -1;
        public float latitude = -1;
        public float altitude = -1;

        public GPSCoordinates(double theLongitude, double theLatitude, double theAltitude) {
            longitude = (float) theLongitude;
            latitude = (float) theLatitude;
            altitude = (float) theAltitude;
        }
    }
}
