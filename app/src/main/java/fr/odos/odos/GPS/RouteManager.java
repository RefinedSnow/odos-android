package fr.odos.odos.GPS;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by gabon on 17/01/18.
 */

public class RouteManager {

    private GPSLocation currentLocation;
    private LatLng destination;
    private GoogleMap mMap;
    private DownloadTask dlTask;


    public RouteManager(GoogleMap map, GPSLocation current){
        mMap = map;
        currentLocation = current;
        getRoute();
    }


    public void getRoute(){

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                if (currentLocation == null)
                {
                    return;
                }

                if (destination != null) {
                    destination = null;
                    mMap.clear();
                }

                destination = latLng;

                // Creating MarkerOptions
                MarkerOptions options = new MarkerOptions();

                // Setting the position of the marker
                options.position(destination);


                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));


                // Add new marker to the Google Map Android API V2
                mMap.addMarker(options);


                // Getting URL to the Google Directions API
                String url = getDirectionsUrl(new LatLng(currentLocation.getLongitude(), currentLocation.getLatitude()), destination);

                DownloadTask downloadTask = new DownloadTask(mMap);
                dlTask = downloadTask;

                // Start downloading json data from Google Directions API
                downloadTask.execute(url);


            }
        });
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=walking";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }


}
