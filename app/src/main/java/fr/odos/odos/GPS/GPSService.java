package fr.odos.odos.GPS;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;


public class GPSService {

    private GPSLocation gpsLocation;
    private IGPS igpsListener;


    public void getGPSLocation(Context context){

        LocationProvider.requestSingleUpdate(context,
                new LocationProvider.LocationCallback() {
                        @Override
                        public void onNewLocationAvailable(LocationProvider.GPSCoordinates location) {
                             gpsLocation = new GPSLocation( location.longitude,
                                                            location.latitude,
                                                            location.altitude);

                           // System.out.println(location.latitude+" "+location.longitude+" "+location.altitude);

                               new Handler(Looper.getMainLooper()).post(new Runnable(){
                                 @Override
                                 public void run() {
                                     igpsListener.onLocationChange(gpsLocation);
                                 }
                             });

                        }
                    });
    }

    public void setGPSListener(IGPS igps){

        this.igpsListener = igps;
    }



}