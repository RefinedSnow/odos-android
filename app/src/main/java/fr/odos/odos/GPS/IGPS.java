package fr.odos.odos.GPS;

/**
 * Created by gabon on 16/01/18.
 */

public interface IGPS {

    public void onLocationChange(GPSLocation gpsLocation);
}
