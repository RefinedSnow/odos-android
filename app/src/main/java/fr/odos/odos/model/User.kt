package fr.odos.odos.model


/**
 * Created by Jen on 16.01.2018.
 */

class User constructor(var userId:Int, var userNickname:String, var userEmail: String, var userScore:Int){
    var userSuccesses: MutableList<Success>? = null
    var userRewards: MutableList<Reward>? = null

    fun addSuccess(success: Success){
        if(userSuccesses == null)
        {
            userSuccesses = mutableListOf()
        }
        userSuccesses!!.add(success)
    }

    fun addReward(reward: Reward){
        if(userRewards == null){
            userRewards = mutableListOf()
        }
        userRewards!!.add(reward)
    }

    fun addScore(points:Int){
        val MAX_SCORE_VALUE = 99999
        if(points < 0)
        {
            if(userScore <0 || (userScore + points) < 0){
                userScore = 0
            }
        }
        else if ( userScore < MAX_SCORE_VALUE)
        {
            if(userScore + points < MAX_SCORE_VALUE) {
                userScore += points
            }
            else {
                userScore = MAX_SCORE_VALUE
            }
        }
    }
}