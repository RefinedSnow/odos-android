package fr.odos.odos.model;

import android.app.Application;

import java.util.ArrayList;

/**
 * Created by Thibaut on 18/01/2018.
 */

public class Data extends Application{

    private static Data mInstance = null;
    private String _email;
    private ArrayList<String> _petList;
    private ArrayList<String> _skinList;
    private ArrayList<String> _hairList;
    private ArrayList<String> _clothesList;
    private ArrayList<Integer> _stateButtonPet;
    private int skin;
    private int hair;
    private int shirt;
    private int pet;
    private int score;
    private int distance;
    private String pseudo;
    private Boolean _exploreur;
    private String message = "";

    int MAXPET = 10;
    int MAXSKIN = 3;
    int MAXHAIR = 5;
    int MAXCLOTHES = 5;

    protected Data(){
        _exploreur = false;
        //score = 300;
        _petList = new ArrayList<>(MAXPET);
        _hairList = new ArrayList<>(MAXHAIR);
        _skinList = new ArrayList<>(MAXSKIN);
        _clothesList = new ArrayList<>(MAXCLOTHES);

        _petList.add("vide");
        _petList.add("dragonra");
        _petList.add("dragonrb");
        _petList.add("dragonrc");
        _petList.add("dragonva");
        _petList.add("dragonvb");
        _petList.add("dragonvc");
        _petList.add("dragonna");
        _petList.add("dragonnb");
        _petList.add("dragonnc");

        _skinList.add("perso");
        _skinList.add("perso_deux");
        _skinList.add("perso_trois");

        _hairList.add("cheveux_un");
        _hairList.add("cheveux_deux");
        _hairList.add("cheveux_trois");
        _hairList.add("cheveux_quatre");
        _hairList.add("cheveux_cinq");
        _hairList.add("habit_un");

        _clothesList.add("habit_deux");
        _clothesList.add("habit_trois");
        _clothesList.add("habit_quatre");
        _clothesList.add("habit_cinq");
        _clothesList.add("habit_six");
        _stateButtonPet = new ArrayList<>(10);
        for (int i = 0; i< 10; i++){
            _stateButtonPet.add(0);
        }
    }

    public void set_email(String email){
        this._email = email;
    }

    public String get_email(){
        return this._email;
    }

    public ArrayList<String> get_petList(){
        return _petList;
    }

    public ArrayList<String> get_skinList(){
        return _skinList;
    }

    public ArrayList<String> get_hairList(){
        return _hairList;
    }
    public ArrayList<String> get_clothesList(){
        return _clothesList;
    }

    public static synchronized Data getInstance(){
        if(null == mInstance){
            mInstance = new Data();
        }
        return mInstance;
    }

    public int getMaxSkin() {
        return MAXSKIN;
    }

    public int getMaxHair() {
        return MAXHAIR;
    }

    public int getMaxClothes() {
        return MAXCLOTHES;
    }

    public int getMaxPet(){
        return MAXPET;
    }

    public int getHair() {
        return hair;
    }

    public void setHair(int hair) {
        this.hair = hair;
    }

    public int getSkin() {
        return skin;
    }

    public void setSkin(int skin) {
        this.skin = skin;
    }

    public int getShirt() {
        return shirt;
    }

    public void setShirt(int shirt) {
        this.shirt = shirt;
    }

    public int getPet() {
        return pet;
    }

    public void setPet(int pet) {
        this.pet = pet;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public ArrayList<Integer> get_stateButtonPet() {
        return _stateButtonPet;
    }

    public void set_stateButtonPet(ArrayList<Integer> _stateButtonPet) {
        this._stateButtonPet = _stateButtonPet;
    }

    public Boolean get_exploreur() {
        return _exploreur;
    }

    public void set_exploreur(Boolean _exploreur) {
        this._exploreur = _exploreur;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
