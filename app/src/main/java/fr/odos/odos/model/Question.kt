package fr.odos.odos.model

/**
 * Created by Jen on 16.01.2018.
 */

class Question constructor(var questionId:Int, var questionLabel:String, var questionValue:Int, var questionAnswer: Answer){
    var questionAnswers: MutableList<Answer>?=null

    fun setAnswers(answers:MutableList<Answer>){
        questionAnswers = answers
    }

    fun addAnswer(answer: Answer){
        if(questionAnswer == null)
        {
            questionAnswers = mutableListOf<Answer>()
        }
        questionAnswers!!.add(answer)
    }

}