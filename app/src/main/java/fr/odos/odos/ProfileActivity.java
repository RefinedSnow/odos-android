package fr.odos.odos;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import fr.odos.odos.model.Data;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        String email = Data.getInstance().get_email();

        ImageView iLight = (ImageView) findViewById(R.id.iLight);
        iLight.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          Intent intent = new Intent(ProfileActivity.this, PetActivity.class);
                                          finish();
                                          startActivity(intent);
                                      }
                                  });

        Button bBack = (Button) findViewById(R.id.bBack);
        bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, MapActivity.class);
                finish();
                startActivity(intent);
            }
        });

        Button bSetting = (Button) findViewById(R.id.bSettings);
        bSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, SettingsActivity.class);
                finish();
                startActivity(intent);
            }
        });

        initViews();
    }

    public void initViews() {

        ArrayList<String> listPet = Data.getInstance().get_petList();
        ArrayList<String> skinName = Data.getInstance().get_skinList();
        ArrayList<String> hairName = Data.getInstance().get_hairList();
        ArrayList<String> clothesName = Data.getInstance().get_clothesList();;

        TextView tVPseudo = (TextView) findViewById(R.id.tVPseudo);
        TextView tVDistance = (TextView) findViewById(R.id.tVDistance);
        TextView tVCoin = (TextView) findViewById(R.id.tVCoin);

        tVPseudo.setText(Data.getInstance().getPseudo());
        tVDistance.setText(String.valueOf(Data.getInstance().getDistance()));
        tVCoin.setText(String.valueOf(Data.getInstance().getScore()));

        ImageView iv = null;
        int img = 0;
        Drawable d = null;

        iv = (ImageView) findViewById(R.id.iPet);
        img = getResources().getIdentifier(listPet.get(Data.getInstance().getPet()), "drawable", getPackageName());
        d = getResources().getDrawable(img, null);
        iv.setImageDrawable(d);

        iv = (ImageView) findViewById(R.id.iHair);
        img = getResources().getIdentifier(hairName.get(Data.getInstance().getHair()), "drawable", getPackageName());
        d = getResources().getDrawable(img, null);
        iv.setImageDrawable(d);

        iv = (ImageView) findViewById(R.id.iFace);
        img = getResources().getIdentifier(skinName.get(Data.getInstance().getSkin()), "drawable", getPackageName());
        d = getResources().getDrawable(img, null);
        iv.setImageDrawable(d);

        iv = (ImageView) findViewById(R.id.iShirt);
        img = getResources().getIdentifier(clothesName.get(Data.getInstance().getShirt()), "drawable", getPackageName());
        d = getResources().getDrawable(img, null);
        iv.setImageDrawable(d);
    }

}
