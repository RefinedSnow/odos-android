package fr.odos.odos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import fr.odos.odos.model.Data;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final Button bRegister = (Button)  findViewById(R.id.bConnection);
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registered();
            }
        });

        final Button bBack = (Button)  findViewById(R.id.bBack);
        bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        });

    }


    public void registered(){
        //Toast.makeText(this, "test", Toast.LENGTH_SHORT).show();
        EditText eTEmail = (EditText) findViewById(R.id.etEmail);
        EditText eTpseudo = (EditText) findViewById(R.id.eTPseudo);
        EditText eTpass1 = (EditText) findViewById(R.id.eTPassword1);
        EditText eTpass2 = (EditText) findViewById(R.id.eTPassword2);

        String email = eTEmail.getText().toString();
        String pseudo = eTpseudo.getText().toString();
        String pass1 = eTpass1.getText().toString();
        String pass2 = eTpass2.getText().toString();

        if(Objects.equals(email, "") || Objects.equals(pseudo, "") || Objects.equals(pass1, "") || Objects.equals(pass2, "")){
            Toast.makeText(this, "@string/imcompleteField", Toast.LENGTH_SHORT).show();
            return;
        }else if (!Objects.equals(pass1, pass2)){
            Toast.makeText(this, "@string/differentPassword", Toast.LENGTH_SHORT).show();
            return;
        }

        Data.getInstance().set_email(email);

        MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient okHttpClient = new OkHttpClient();

        JSONObject json = new JSONObject();
        try {
            json.put("email", email);
            json.put("nickname", pseudo);
            json.put("password", pass1);
            Request myGetRequest = new Request.Builder()
                    .url("https://dome7.ensicaen.fr:8443/api/register")
                    .post(RequestBody.create(JSON_TYPE, json.toString()))
                    .build();

            okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    final String text = response.body().string();
                    Log.e("Json",text);
                    try {

                        JSONObject obj = new JSONObject(text);
                        String val =  obj.getString("success");
                        String message = obj.getString("message");


                        if (val == "true"){
                            Intent intent = new Intent(RegisterActivity.this, PersonalisationActivity.class);
                            finish();
                            startActivity(intent);
                        }else{
                            Data.getInstance().setMessage(message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(LoginActivity.this, text, Toast.LENGTH_SHORT).show();
                            String message = Data.getInstance().getMessage();
                            if(message != ""){
                                Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                                Data.getInstance().setMessage("");
                            }
                        }
                    });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return;
    }


}
