package fr.odos.odos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import fr.odos.odos.model.Data;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Button bLogin = (Button) findViewById(R.id.bLogin);

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    checkIdentity();
            }
        });

        final Button bRegister = (Button) findViewById(R.id.bRegister);

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                finish();
                startActivity(intent);
            }
        });

        final TextView bForget = (TextView) findViewById(R.id.tForget);

        bForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                finish();
                startActivity(intent);

            }
        });

    }

    private void checkIdentity(){
        final EditText email = (EditText) findViewById(R.id.eTLogin);
        final EditText password = (EditText) findViewById(R.id.eTPassword);

        String tEmail = email.getText().toString();
        String tPassword = password.getText().toString();

        if(tEmail.equals("") || tEmail.equals("")){
            Toast.makeText(this, "@string/imcompleteField", Toast.LENGTH_SHORT).show();
            return;
        }

        Data.getInstance().set_email(tEmail);

        MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient okHttpClient = new OkHttpClient();

        JSONObject json = new JSONObject();
        try {
            json.put("email", tEmail);
            json.put("password", tPassword);
            Request myGetRequest = new Request.Builder()
                    .url("https://dome7.ensicaen.fr:8443/api/authenticate")
                    .post(RequestBody.create(JSON_TYPE, json.toString()))
                    .build();

            okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    final String text = response.body().string();
                    try {

                        JSONObject obj = new JSONObject(text);
                        String val =  obj.getString("success");
                        String message = obj.getString("message");
                        if (val == "true"){
                            Intent intent = new Intent(LoginActivity.this, MapActivity.class);
                            startActivity(intent);
                        }else{
                            Data.getInstance().setMessage(message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(LoginActivity.this, text, Toast.LENGTH_SHORT).show();
                            String message = Data.getInstance().getMessage();
                            if(message != ""){
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                Data.getInstance().setMessage("");
                            }
                        }
                    });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
