package fr.odos.odos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import fr.odos.odos.model.Data;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        Switch sExploreur = (Switch) findViewById(R.id.sExploreur);
        sExploreur.setChecked(Data.getInstance().get_exploreur());
        sExploreur.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Data.getInstance().set_exploreur(isChecked);
                Toast.makeText(SettingsActivity.this, "Fonctionalité non disponible actuellement", Toast.LENGTH_SHORT).show();
            }
        });

        Button bBack = (Button) findViewById(R.id.bBack);
        bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, MapActivity.class);
                finish();
                startActivity(intent);
            }
        });

        Button bLogOut = (Button) findViewById(R.id.bLogOut);
        bLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }
}
